# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'structure_compost',
    'version': '1.1',
    'summary': 'structure_compost',
    'author': 'FATMA EZZAHRA BEN HMIDA',
    'description': """
    Structure_compost
    """,
    'depends': ['base', 'account','sale','product','stock','crm','account','website_sale','delivery','dp_eu_gdpr'],
    'data': [
    'data/account_data.xml',
    'views/structure_assets_backend.xml',
    'views/sale_report.xml',
    'views/report_devis_sale.xml',
    'views/report_devis_sale_avec_contrat.xml',
    'views/report_devis_sale_sans_contrat.xml',
    'views/report_facture_invoice.xml',
    'views/structure_compost_views.xml',
    'views/res_partner_views.xml',
    
    

    ],

    'installable': True,
    'auto_install': False,
}
