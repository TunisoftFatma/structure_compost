# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
import time

class structure_compost(models.Model):
    _name = "structure.compost"

    name = fields.Char('')


class ResPartner(models.Model):
	_inherit = 'res.partner'
 
	prospects=fields.Boolean('Est un Prospect')


class ResCompany(models.Model):
    
    _inherit = 'res.company'
    
    
    report_facture=fields.Text('Pied de page de rapport facture')


class AccountInvoice(models.Model):
	_inherit = "account.invoice"

	def facture_accompte(self):
		objsale=''
		list={}
		list_product=[]
		list_livraison=[]
		taxsale=0
		if self.name:
			objsale=self.env['sale.order'].search([('name','=',self.name)])
			print('amount tax',objsale.amount_tax)
		if objsale:
			taxsale=objsale.amount_tax
			for i in self.env['sale.order.line'].search([('order_id','=',objsale.id)]):
				delivery_method_ids =self.env['delivery.carrier'].search([('product_id','=',i.product_id.id)])
				if not i.product_id.property_account_income_id.id and not delivery_method_ids:
					
					name=i.product_id.name
					quantity=i.product_uom_qty
					price_unit=i.price_unit
					tax=', '.join(map(lambda x: (x.description or x.name), i.tax_id))
					price_total=i.price_subtotal
					montant=i.price_total
					price_tax=i.tax_id.amount
					list_product.append({'name':name,'quantity':quantity,'price_unit':price_unit,'tax':tax,'price_total':price_total,'montant':montant,'price_tax':price_tax})
				if 	delivery_method_ids :
					list_livraison.append({'name':i.product_id.name,'price_unit':i.price_subtotal,'price_total':i.price_total})
		list={'list_product': list_product,'list_livraison':list_livraison,'taxsale':taxsale}
		return list

	def different_condition_facture(self, invoice_line):
		list={}
		id_pr=invoice_line.product_id
		type_de_cas='article'
		delivery_method_ids =self.env['delivery.carrier'].search([('product_id','=',id_pr.id)])
		# cas facture article + livraison + acompte
		# cas facture article + livraison
		if delivery_method_ids:
			type_de_cas='livraison'
		# cas facture acompte sans livraison
		if id_pr.property_account_income_id.id  :
			if self.name:
				type_de_cas='acompte'
			else:
				type_de_cas='acomptesolde'
			# if  not delivery_method_ids:
			# 	type_de_cas='acomptewithoutlivraison'
		list={'type_de_cas':type_de_cas}
		return list
	
	def check_advance_payment(self, invoice_line):
		id_pr=invoice_line.product_id
		varrt=False
		if id_pr.property_account_income_id.id:
			varrt=True
		return varrt

class SaleAdvancePaymentInv(models.TransientModel):
	_inherit = "sale.advance.payment.inv"

	@api.multi
	def create_invoices(self):
	    sale_orders = self.env['sale.order'].browse(self._context.get('active_ids', []))

	    if self.advance_payment_method == 'delivered':
	        sale_orders.action_invoice_create()
	    elif self.advance_payment_method == 'all':
	        sale_orders.action_invoice_create(final=True)
	    else:
	        # Create deposit product if necessary
	        if not self.product_id:
	            vals = self._prepare_deposit_product()
	            self.product_id = self.env['product.product'].create(vals)
	            self.env['ir.config_parameter'].sudo().set_param('sale.default_deposit_product_id', self.product_id.id)

	        sale_line_obj = self.env['sale.order.line']
	        for order in sale_orders:
	            if self.advance_payment_method == 'percentage':
	                amount = order.amount_untaxed * self.amount / 100
	            else:
	                amount = self.amount
	            if self.product_id.invoice_policy != 'order':
	                raise UserError(_('The product used to invoice a down payment should have an invoice policy set to "Ordered quantities". Please update your deposit product to be able to create a deposit invoice.'))
	            if self.product_id.type != 'service':
	                raise UserError(_("The product used to invoice a down payment should be of type 'Service'. Please use another product or update this product."))
	            taxes = self.product_id.taxes_id.filtered(lambda r: not order.company_id or r.company_id == order.company_id)
	            if order.fiscal_position_id and taxes:
	                tax_ids = order.fiscal_position_id.map_tax(taxes).ids
	            else:
	                tax_ids = taxes.ids
	            so_line = sale_line_obj.create({
	                'name': _("Down payment of %s%%") % (self.amount,),
	                'price_unit': amount,
	                'product_uom_qty': 0.0,
	                'order_id': order.id,
	                'discount': 0.0,
	                'product_uom': self.product_id.uom_id.id,
	                'product_id': self.product_id.id,
	                'tax_id': [(6, 0, tax_ids)],
	                'is_downpayment': True,
	            })
	            self._create_invoice(order, so_line, amount)
	    if self._context.get('open_invoices', False):
	        return sale_orders.action_view_invoice()
	    return {'type': 'ir.actions.act_window_close'}
